module.exports = {
    artifactsDir: '.',
    sourceDir: './dist',
    lint: {
        warningsAsErrors: false,
    },
    run: {
        firefox: 'firefoxdeveloperedition',
        startUrl: ['about:debugging#/runtime/this-firefox'],
    },
};
