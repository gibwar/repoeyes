import browser from 'webextension-polyfill';
import { LitElement, html } from 'lit';
import { customElement } from 'lit/decorators.js';

console.log('hi!');

@customElement('the-app')
class TheApp extends LitElement {
    render() {
        return html`
            <h1>Hello!</h1>
            <p>This is element is lit!</p>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'the-app': TheApp;
    }
}

const app = document.createElement('the-app');
document.body.appendChild(app);

async function start() {
    const result = await browser.runtime.sendMessage({ type: 'auth', action: 'login', key: 'abc123' });
    console.log(result);
}

start();
