import { Message, MessageHandler } from './handler';

export class AuthMessageHandler extends MessageHandler {
    readonly _name = 'auth';

    async login({ key }: { key: string }) {
        return { success: true, name: 'username', source: 'github', key };
    }

    logout() { /* a */ }

    async refresh() { /* a */ }
}

declare global {
    interface IMessageHandlerMap {
        auth: AuthMessageHandler;
    }
}

declare module 'webextension-polyfill' {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Runtime {
        interface Static {
            sendMessage<T extends Message<AuthMessageHandler, 'login'>>(message: T['input']): T['output'];
            sendMessage<T extends Message<AuthMessageHandler, 'logout'>>(message: T['input']): T['output'];
            sendMessage<T extends Message<AuthMessageHandler, 'refresh'>>(message: T['input']): T['output'];
        }
    }
}
