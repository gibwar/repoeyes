import { BaseHandler } from '../util/baseHandler';
import { HandlerManager } from '../util/handlerManager';

import { AuthMessageHandler } from './AuthMessageHandler';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface IMessageHandlerMap { }
}

export type MessageBase = {
    type: keyof IMessageHandlerMap;
    action: string;
};

export abstract class MessageHandler extends BaseHandler {
    handle(message: MessageBase, sender: browser.Runtime.MessageSender): Promise<unknown> | void {
        const action = message.action as Exclude<keyof MessageHandler, '_name'>;
        return this[action](message, sender);
    }
}

export type Message<
    T extends MessageHandler,
    A extends keyof T,
    N = T['_name'],
    P = T[A] extends (...args: infer A) => unknown ? A[0] : never,
    S = { type: N; action: A },
    I = P extends Record<string, unknown> ? S & P : S,
    O = T[A] extends (...args: never) => infer R ? R : never // maybe Promise<void>?
> = {
    input: I
    output: O;
};

export function buildDefaultMessageHandler(): HandlerManager<IMessageHandlerMap, MessageHandler> {
    const manager = new HandlerManager<IMessageHandlerMap, MessageHandler>();

    manager.register(AuthMessageHandler);

    manager.markReady();

    return manager;
}
