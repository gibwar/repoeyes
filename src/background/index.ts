import browser from 'webextension-polyfill';

import { buildDefaultMessageHandler, MessageBase } from './messages/handler';

const messageHandler = buildDefaultMessageHandler();

browser.browserAction.onClicked.addListener(async () => {
    await browser.tabs.create({
        url: '/ui.html',
        active: true,
    });
});

browser.runtime.onMessage.addListener(async (message: MessageBase, sender) => {
    const handler = messageHandler.get(message.type);
    return await handler.handle(message, sender);
});
