import { BaseHandler } from './baseHandler';

type AddParams = Parameters<EventTarget['addEventListener']>;
type RemoveParams = Parameters<EventTarget['removeEventListener']>;

/**
 * Provides an implementation of registering a set of handlers with names for fast retrevial and
 * provides type safety when working with handlers.
 *
 * @template HandlerMap - Handler mapping interface mapping keys to Handler implementations.
 * @template  Handler - Describes handler implementations that registered handler clases must
 * implement to be registered.
 * @template K - Used to strongly index into the HandlerMap interface and provide type checking when
 * accessing and returning registered handlers.
 */
export class HandlerManager<
    HandlerMap extends Record<K, Handler>,
    Handler extends BaseHandler = BaseHandler,
    K extends keyof HandlerMap = keyof HandlerMap
> {
    private readonly _map: Map<unknown, HandlerMap[K]> = new Map();
    private readonly _event = new EventTarget();
    private _ready = false;

    /**
     * Gets one or more registered handler within the map. Throws if the handler is not registered.
     * @arg keys - Name of the handler to retrieve, must be in HandlerMap.
     * @returns Returns the registered handler instance or an object mapping keys to handlers.
     */
    get<R extends keyof HandlerMap>(key: R): HandlerMap[R]
    get<R extends keyof HandlerMap>(...keys: R[]): Pick<HandlerMap, R>
    get<R extends keyof HandlerMap>(keys: R | R[]): HandlerMap[R] | Pick<HandlerMap, R> {
        if (!Array.isArray(keys)) {
            const handler = this._map.get(keys);
            if (!handler) {
                throw new Error(`Handler ${keys} not registered`);
            }

            return handler as unknown as HandlerMap[R];
        }

        const result: Partial<Pick<HandlerMap, R>> = {};

        for (const key of keys) {
            result[key] = this.get(key);
        }

        return result as Required<Pick<HandlerMap, R>>;
    }

    /** Marks the handler as ready, if needed. Throws if called more than once. */
    markReady(): void {
        if (this._ready) {
            throw new Error('This manager has already been marked as ready.');
        }

        this._ready = true;
        this._event.dispatchEvent(new Event('ready', { cancelable: false }));
    }

    /**
     * Unregisters an event listener from the internal EventTarget.
     */
    off (type: RemoveParams[0], callback: RemoveParams[1], options?: RemoveParams[2]): void {
        if (options) {
            this._event.removeEventListener(type, callback, options);
        }
        else {
            this._event.removeEventListener(type, callback);
        }
    }

    /**
     * Registers an event listener with the internal EventTarget. If a single parameter is provided
     * a promise is returned that will be triggered once when the event is raised. If a callback is
     * provided, the listener will be called accoridng to the options passed.
     */
    on(type: AddParams[0]): Promise<void>
    on(type: AddParams[0], callback: AddParams[1], options?: AddParams[2]): void
    on(
        type: AddParams[0],
        callbackOrOptions?: AddParams[1] | AddParams[2],
        options?: AddParams[2]
    ): Promise<void> | void {
        if (callbackOrOptions && options && typeof callbackOrOptions === 'function') {
            this._event.addEventListener(type, callbackOrOptions, options);
        }
        else if (typeof callbackOrOptions === 'function') {
            this._event.addEventListener(type, callbackOrOptions);
        }

        return new Promise((resolve) => {
            this._event.addEventListener(type, () => resolve(), { once: true });
        });
    }

    /**
     * Raises an event on the internal EventTarget.
     * @arg event - Event to dispatch to potential listeners.
     */
    raise(event: Event): void {
        this._event.dispatchEvent(event);
    }

    /** Utility method to wait until the manager is marked as ready. */
    ready(): Promise<void> {
        if (this._ready) {
            return Promise.resolve();
        }

        return this.on('ready');
    }

    /** Registers a new handler with the manager. */
    register<
        T extends { new(manager: HandlerManager<HandlerMap, Handler, K>): HandlerMap[K] }
    >(cls: T): void {
        const handler = new cls(this);
        const key = handler._name;

        if (this._map.has(key)) {
            throw new Error(`Handler ${handler._name} already registered with this manager.`);
        }

        this._map.set(key, handler);
        this._event.dispatchEvent(new Event(`${key}`, { cancelable: false }));
    }

    /** Waits for the requested handler to be registered or returns immediately if it already is. */
    when<R extends keyof HandlerMap>(key: R): Promise<HandlerMap[R]> {
        if (this._map.has(key)) {
            return Promise.resolve(this.get(key));
        }

        return new Promise((resolve) => {
            this._event.addEventListener(`${key}`, () => resolve(this.get(key)), { once: true });
        });
    }
}
