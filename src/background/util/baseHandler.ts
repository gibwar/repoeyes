/**
* Abstract base class representing a handler that can be registered with the HandlerManager class.
*/
export abstract class BaseHandler {
    /** Name of the handler to use in the handler map. */
    abstract readonly _name: string;
}
