import esbuild from 'esbuild';
import fs from 'fs-extra';

/**
 * @param {Record<string, string>} options
 * @returns {esbuild.Plugin}
 */
const copyPlugin = (options) => {
    /** @type {esbuild.Plugin} */
    const plugin = {
        name: 'copy',
        setup(build) {
            build.onEnd(async (result) => {
                if (result.errors.length) {
                    return;
                }

                console.log('Copying assets...');

                for (const [src, dest] of Object.entries(options)) {
                    await fs.copy(src, dest, { overwrite: true, recursive: true, filter: (s, d) => {
                        console.log(`  ${s} -> ${d}`.replace(/\\/g, '/'));
                        return true;
                    }});
                }
            });
        },
    };

    return plugin;
};

/**
 * @param {{entry: string; output: string; title?: string; body: string?}[]} options
 * @returns {esbuild.Plugin}
 */
const htmlPlugin = (options) => {
    /** @type {esbuild.Plugin} */
    const plugin = {
        name: 'html',
        setup(build) {
            build.onEnd(async (result) => {
                if (!result.metafile) {
                    return;
                }

                for (const config of options) {
                    const matched = Object.entries(result.metafile.outputs)
                        .filter(([_, value]) => value.entryPoint === config.entry)
                        .map(([file]) => file);

                    const { outdir } = build.initialOptions;
                    const scripts = matched.filter((file) => /js$/.test(file)).map(file => file.replace(outdir, '/'));

                    const file = `<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8" />
    <title>${config.title ?? ''}</title>
    ${scripts.map(file => `<script type="module" src="${file}"></script>`)}
  <head>
  <body>${config.body ?? ''}</body>
</html>
`;

                    await fs.writeFile(`${outdir}/${config.output}`, file, { encoding: 'utf8' });
                }
            });
        }
    };

    return plugin;
};

/** @type {esbuild.BuildOptions} */
const options = {
    bundle: true,
    color: true,
    entryNames: '[dir]/[name]-[hash]',
    entryPoints: ['src/background/index.ts', 'src/ui/index.ts'],
    format: 'esm',
    legalComments: 'linked',
    metafile: true,
    outExtension: { '.js': '.mjs', },
    outbase: 'src/',
    outdir: 'dist/',
    platform: 'browser',
    plugins: [
        copyPlugin({ 'assets': 'dist', 'src/manifest.json': 'dist/manifest.json' }),
        htmlPlugin([{
            entry: 'src/background/index.ts',
            output: 'background.html',
        }, {
            entry: 'src/ui/index.ts',
            output: 'ui.html',
        }]),
    ],
    sourcemap: 'inline',
    target: 'es2020',
    treeShaking: true,
};

await fs.remove('dist');

try {
    const result = await esbuild.build(options);
    const text = await esbuild.analyzeMetafile(result.metafile, { color: true, verbose: true });
    console.log(text);
}
catch {
    // esbuild presents nicer output than node's unhandled exception output.
}
